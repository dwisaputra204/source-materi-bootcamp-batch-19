var hobbies = ["coding", "cycling", "climbing", "skateboarding"]

var words = ["halo", "dunia", "ini", "saya"]

// menggabungkan data di dalam array menjadi sebuah string (satu kalimat)
// console.log(words.join("-"))

// mengurutkan data
words.sort()
console.log(words)


// menambahkan data di belakang
// hobbies.push("football")

// menghapus data di belakang
// hobbies.pop()

// menambahkan data di depan
// hobbies.unshift("football")

// menghapus data di depan
hobbies.shift()

console.log(hobbies)

var fruits = [ "banana", "orange", "grape"]

fruits.splice(1, 1, "watermolen")

console.log(fruits)

var str = "halo-dunia"

var arrOfStr = str.split("-")

console.log(arrOfStr)