// contoh while

//contoh 1

var angka = 1;

while(angka < 10){
  console.log(angka+ ". sekarang urutan ke-"+ angka);
  angka++;
}


// contoh 2
var deret = 5;
var jumlah = 0;
while(deret > 0) { // Loop akan terus berjalan selama nilai deret masih di atas 0
  jumlah += deret; // Menambahkan nilai variable jumlah dengan angka deret
  deret--; // Mengubah nilai deret dengan mengurangi 1
  console.log('Jumlah saat ini: ' + jumlah)
}
 
console.log(jumlah);

// contoh for loop
// contoh 1
for(var iterasi = 1; iterasi<10; iterasi++){
    console.log(iterasi+ ". sekarang urutan ke-"+ iterasi);
}

// contoh 2

var jumlah = 0;
for(var deret = 5; deret > 0; deret--) {
  jumlah += deret;
  console.log('Jumlah saat ini: ' + jumlah);
}
 
console.log('Jumlah terakhir: ' + jumlah);