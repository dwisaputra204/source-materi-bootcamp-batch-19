import React, { useContext } from "react"
import {Link} from "react-router-dom";
import { Layout, Menu } from 'antd';
import {UserContext} from "../Materi-17/UserContext"

const { Header } = Layout;

const Nav = () =>{
  const [user, setUser] = useContext(UserContext)

  const handleLogout = ()=>{
    setUser(null)
    localStorage.removeItem("user")
  }

  return (
    <Header>        
      <Menu className="custom-navbar" theme="dark" mode="horizontal">
        <Menu.Item>
          <Link to="/">Materi 9</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-10-1">Materi 10 - 1</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-10-2">Materi 10 - 2</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-11">Materi 11</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-12">Materi 12</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-13-1">Materi 13 - 1</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-13-2">Materi 13 - 2</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-14">Materi 14</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/materi-16">Materi 16</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/peserta-with-auth">Peserta With Auth - 17</Link>
        </Menu.Item>
        {user === null && (
          <>
            <Menu.Item>
              <Link to="/register">Register</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/login">Login</Link>
            </Menu.Item>
          </>
        )}

        {user  && 
          <Menu.Item>
            <span onClick={handleLogout}>Logout</span>
          </Menu.Item>
        }
      </Menu>
    </Header>
    
  )
}

export default Nav


