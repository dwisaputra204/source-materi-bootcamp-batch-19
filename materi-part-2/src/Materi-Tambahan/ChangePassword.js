import React, { useState, useContext } from "react"
import { UserContext } from "../Materi-17/UserContext"
import axios from "axios"

const ChangePassword = ()=>{
  const [user] = useContext(UserContext)
  const [input, setInput] = useState({current_password: "", new_password: "", new_confirm_password: ""})
  const handleSubmit = (event) =>{
    event.preventDefault()
    axios.post(`https://backendexample.sanbersy.com/api/change-password`, {current_password: input.current_password, new_password: input.new_password, new_confirm_password: input.new_confirm_password}, {headers: {"Authorization" : `Bearer ${user.token}`}})
    .then(res => {
        alert("success")
        setInput({current_password: "", new_password: "", new_confirm_password: ""})
      }).catch((err)=>{
        alert("salah input")
        setInput({current_password: "", new_password: "", new_confirm_password: ""})
    })

  }
  
  const handleChange = (event)=>{
    let typeOfInput = event.target.name
    let value = event.target.value
    
    if (typeOfInput === "current_password"){
      setInput({...input, current_password: value})
    }else if (typeOfInput === "new_password"){
      setInput({...input, new_password: value})
    }else if (typeOfInput === "new_confirm_password"){
      setInput({...input, new_confirm_password: value})
    }

  }

  return(
    <>
    <h1>Change Password</h1>
    <form style={{paddingBottom: "20px"}}onSubmit={handleSubmit}>
      <label>
        password lama:
      </label>          
      <input type="password" name="current_password" value={input.current_password} onChange={handleChange}/>
      <br/>
      <label>
        password baru:
      </label>          
      <input type="password" name="new_password" value={input.new_password} onChange={handleChange}/>
      <br/>
      <label>
        konfirmasi password baru:
      </label>          
      <input type="password" name="new_confirm_password" value={input.new_confirm_password} onChange={handleChange}/>
      <br/>
      <button>submit</button>
    </form>
  </>
  )
}

export default ChangePassword